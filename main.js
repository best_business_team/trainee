//caralhoooojeiejpoe
var AutoUpdater = require('auto-updater');
 
    var autoupdater = new AutoUpdater({
     pathToJson: '',
     autoupdate: false,
     checkgit: true,
     jsonhost: 'https://bitbucket.org/best_business_team/trainee/raw/52f1ea9984d26d4f537dbfa1b221ebe7cd9ecbd0/package.json',
     contenthost: 'https://bitbucket.org/best_business_team/trainee/get/66e4a5b56053.zip',
     progressDebounce: 0,
     devmode: false
    });
 
    // State the events 
    autoupdater.on('check.up-to-date', function(v) {
      console.info("You have the latest version: " + v);
    });
    autoupdater.on('check.out-dated', function(v_old, v) {
      console.warn("Your version is outdated. " + v_old + " of " + v);
      autoupdater.fire('download-update'); // If autoupdate: false, you'll have to do this manually. 
      // Maybe ask if the'd like to download the update. 
    });
    autoupdater.on('update.downloaded', function() {
      console.log("Update downloaded and ready for install");
      autoupdater.fire('extract'); // If autoupdate: false, you'll have to do this manually. 
    });
    autoupdater.on('update.not-installed', function() {
      console.log("The Update was already in your folder! It's read for install");
      autoupdater.fire('extract'); // If autoupdate: false, you'll have to do this manually. 
    });
    autoupdater.on('update.extracted', function() {
      console.log("Update extracted successfully!");
      console.warn("RESTART THE APP!");
    });
    autoupdater.on('download.start', function(name) {
      console.log("Starting downloading: " + name);
    });
    autoupdater.on('download.progress', function(name, perc) {
      process.stdout.write("Downloading " + perc + "% \033[0G");
    });
    autoupdater.on('download.end', function(name) {
      console.log("Downloaded " + name);
    });
    autoupdater.on('download.error', function(err) {
      console.error("Error when downloading: " + err);
    });
    autoupdater.on('end', function() {
      console.log("The app is ready to function");
    });
    autoupdater.on('error', function(name, e) {
      console.error(name, e);
    });
 
    // Start checking 
    autoupdater.fire('check');